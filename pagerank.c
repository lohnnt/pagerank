#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <immintrin.h>
#include <stdbool.h>
#include <string.h>
#include <stddef.h>

#include "pagerank.h"

pthread_barrier_t mybarrier;
bool g_diff = true;
double ESPLION_SQR = EPSILON * EPSILON;

typedef struct{
	// TODO add padding and change the structure of the variables
	size_t id;
	node* a;
	double* result;
	size_t npages;
	size_t nthreads;
	double dampener;
	double * matrix_setup;
	// 8 byte
	char array [8];
} worker_args;


typedef struct{
	size_t id;
	double* a; // contain the npages * npages matrix
	double* b; // contain the npages by 1 matrix
	double* c;
	double* counter;
	size_t npages;
	size_t nthreads;
	// 8 byte padding
	char array[8];
} multi_args;

// Sequential functions
/**
* Compute the difference
*/
// bool s_difference (double * b, double * c, int npages){
// 	double total = 0;
// 	for(int i = 0 ; i < npages ; i++){
// 		total += pow(c[i] - b[i], 2);
// 	}
// 	// printf("Total value is %f to esplion square is %.8lf\n", total, EPSILON*EPSILON);
// 	double root = 0;
// 	root = sqrt(total);
// 	//root = root + round(root * 100.0)/100.0;
// 	// printf("Square rooted total is %f\n", root);
// 	if(root <= EPSILON){
// 		return true;
// 	}
// 	return false;
// }
//
// /**
// * Matrix mulplication
// */
// double * s_matrix_mult(double* a, double* b, int npages){
// 	double* c = (double*) calloc(npages, sizeof(double));
// 	for (int y = 0; y < npages; y++) {
// 		int pos = y * npages;
// 		for (int x = 0; x < npages; x++) {
// 			c[y] += a[ pos +x ] * b[x];
// 		}
// 		// printf("c[%d] has value %.8lf\n", y, c[y]);
// 	}
// 	// printf("\n");
// 	// for(size_t i = 0; i < npages; i++){
// 	// 	printf("at position i has value %.8lf\n", c[i]);
// 	// }
// 	// printf("\n");
// 	return c;
// }

/**
* Searches for if there is any common names in the inlink from the current node.
*/
static bool s_noinlinks(node* inlinks,char name []){
	node* link = inlinks;
	//const char* word = name;
	while (link != NULL){
		if(strcasecmp(link->page->name, name) == 0){
			return true;
		}
		link = link->next;
	}
	return false;
}

// static bool p_noinlinks(char* array, char* name [], int counter){
// 	// look at the nodes to see if they have the correct name
// 	int i = 0;
// 	while(i < counter){
// 		if(strcasecmp(*array[i], *name) == 0){
// 			return true;
// 		}
// 	}
// 	return false;
// }


void* p_matrix_create(void* args){
	worker_args* wargs = (worker_args*) args;
	register size_t npages = wargs->npages;
	register size_t nthreads = wargs->nthreads;
	size_t CHUNK = (npages * npages)/nthreads;
	register size_t id = wargs->id;
	size_t start = id * CHUNK;
	size_t end = id == nthreads - 1 ? npages* npages : start+ 1 * CHUNK;
	node* a = wargs->a;
 	node* b = a;
	bool ninlinks = false;
	double dampener = wargs->dampener;


	// // Find out the exact position of the matrix for x and y
	// // It is setting a and b in the correct starting position
	size_t x = start/npages;
	size_t y = start % npages;
	size_t counter = 0;
	if(y < x){
		// printf("thread start at %d entered the first loop\n", start);
		while (counter < x){
			if(y > counter){
				b = b->next;
			}
			a = a-> next;
			++counter;
		}
	}
	else if(x < y){
		// printf("thread start at %d entered the second loop\n", start);
		while(counter < y){
			if(x > counter){
				a = a->next;
			}
			b = b->next;
			++counter;
		}
	}
	else{
		// x == y
		// printf("thread start at %d entered the third loop\n", start);
		while(counter < x){
			a = a->next;
			b = b->next;
			++counter;
		}
	}
	// counter = 0;
	// char array [npages];
	// node* d = a->page->inlinks;
	// while(d != NULL){
	// 	array[counter] = *d->page->name;
	// 	d = d->next;
	// 	++counter;
	// }
	// printf("At thread # start at %d end at %d has value a %d at and b at%d\n",start,end,x, y);
	double bracket = (((1 - dampener)/ npages) * 1);
	double ratio = dampener * (double) 1 / npages;
	double * result = wargs->result;
	node * c = wargs -> a;
	// size_t numoutlink;
	 for(size_t i = start; i < end; ++i){
		ninlinks = s_noinlinks(a->page->inlinks, b->page->name);
		// ninlinks = p_noinlinks(array, b->page->name, counter);

		result[i] = bracket;

		//numoutlink = b->page->noutlinks;
		if( b->page->noutlinks == (size_t) 0){
			result[i] += ratio;
		}
		else if (ninlinks == true){
			result[i] += dampener * (double) 1/ b->page->noutlinks;
		}
		// else{
		// 	result[i] = bracket;
		// }
		// For traversing at the end of the function
		if(b->next != NULL){
			b = b->next;
		}
		else{
			a = a->next;
			b = c;
		}
	}

	 CHUNK = npages/nthreads;
	 start = id * CHUNK;
	 end = id == nthreads - 1 ? npages : start+ 1 * CHUNK;
	 double* setup = wargs->matrix_setup;
	 for(double* i = setup + start; i < setup + end; ++i){
		*i = (double) 1/ npages;
	 }


	return NULL;
}


// void* p_matrix_create_l(void* args){
// 	worker_args* wargs = (worker_args*) args;
// 	size_t npages = wargs->npages;
// 	size_t nthreads = wargs->nthreads;
// 	size_t CHUNK = npages /nthreads;
// 	size_t id = wargs->id;
// 	size_t start = id * CHUNK;
// 	size_t end = id == nthreads - 1 ? npages: start+ 1 * CHUNK;
// 	node* a = wargs->a;
// 	node*b = a;
// 	bool ninlinks = false;
// 	double dampener = wargs->dampener;
//
//
// 	// // Find out the exact position of the matrix for x and y
// 	// // It is setting a in the correct starting position
// 	size_t x = start/npages;
// 	size_t counter = 0;
// 	while (counter < x){
// 		a = a->next;
// 		++counter;
// 	}
// 	double bracket = (((1 - dampener)/ npages) * 1);
// 	double ratio = dampener * (double) 1 / npages + bracket;
// 	double * result = wargs->result;
// 	double* setup = wargs->matrix_setup;
// 	size_t pos = start * npages;
// 	printf("finish setting up a and other variables\n");
//
// 	for(size_t i = start; i < end; ++i){
// 		// for(node* b = wargs->a; b != NULL; b = b->next){
// 		// 	ninlinks = s_noinlinks(a->page->inlinks, b->page->name);
// 		// 	if(b->page->noutlinks == (size_t) 0){
// 		// 		result[pos] = ratio;
// 		// 	}
// 		// 	else if (ninlinks == true){
// 		// 		result[pos] = dampener * (double) 1/b->page->noutlinks + bracket;
// 		// 	}
// 		// 	else{
// 		// 		result[pos] = bracket;
// 		// 	}
// 		// 	printf("result[%zu] has value %f\n", pos , result[pos]);
// 		// 	++pos;
// 		// }
//
//
// 		for(size_t j = 0; j < npages ;++j){
// 			ninlinks = s_noinlinks(a->page->inlinks, b->page->name);
// 			if(b->page->noutlinks == (size_t) 0){
// 				result[pos +j] = ratio;
// 			}
// 			else if (ninlinks == true){
// 				result[pos + j] = dampener * (double) 1/b->page->noutlinks + bracket;
// 			}
// 			else{
// 				result[pos +j] = bracket;
// 			}
// 			b = b->next;
// 			printf("result[%zu] has value %f\n", pos+j , result[pos+j]);
// 			//++pos;
// 		}
// 		setup[i] = (double) 1/ npages;
// 		b = wargs->a;
// 	}
//
// 	return NULL;
// }


void* p_matrix_mult_1 (void* args){
	multi_args* wargs = (multi_args*) args;
	register size_t npages = wargs->npages;
	register size_t nthreads = wargs->nthreads;
	 size_t CHUNK = npages / nthreads;
	register size_t id = wargs->id;
	 size_t start = id * CHUNK;
	 size_t end = id == nthreads - 1 ? npages : start+ 1 * CHUNK;
	double* a = wargs->a;
	double* b = wargs->b;
	double* c = wargs->c;
	double* counter  = wargs->counter;
	double total, root1, root2, root3, root4;

	// for(double* i = b + start; i < b + end; ++i){
	//    *i = (double) 1/ npages;
	// }

	//bool diff = false;
	while (g_diff){

	counter[id]= 0;

	size_t pos = start*npages;
for(size_t i = start; i < end; ++i){
	for(size_t j = 0; j < npages; ++j){
		c[i] += a[pos] * b[j];
		++pos;
	}
	counter[id] += pow(c[i] - b[i], 2);
	// printf("value at c[%zu] is %.8lf\n", i, c[i]);
}

// size_t b_i = start;
// for(double * i = c + start; i < c + end; ++i){
// 	for(double* j = b; j < b + npages; ++j){
// 		*i = a[pos] * *j;
// 		++pos;
// 	}
// 	counter[id] += pow(*i - b[b_i], 2);
// 	++b_i;
// }

// size_t i = start;
// size_t j = 0;
//
// for(double* ptr_i = c + start; ptr_i < c + end; ++ptr_i){
// 	size_t pos = i * npages;
// 	for(double* ptr_j = b; ptr_j < b + npages; ++ptr_j){
// 		*ptr_i = a[pos + j] * *ptr_j;
// 		++j;
// 	}
// 	j = 0;
// }
		// need a wait for loading balance.
		pthread_barrier_wait(&mybarrier);
// printf("\n");
		 if (id == 0){
			 total = root1 = root2 = root3 = root4 = 0;

			 for(size_t i = 0; i < nthreads; i+=4){
				 root1 += counter[i];
				 root2 += counter[i+1];
				 root3 += counter[i+2];
				 root4 += counter[i+3];
				//  root5 += counter[i+4];
				// root6 += counter[i+5];
			 }
			 total = (root1 + root2) + (root3 + root4);
			//  size_t npages_1 = npages;
			//  for(;npages_1 > 0;--npages_1){
			// 	 root += counter[npages_1];
			//  }

			// printf("Total value is %.8lf\n", root);
			// printf("\n");
			 if(total <= ESPLION_SQR){
				 g_diff = false;
			 }
				//  memset(counter, 0, nthreads * sizeof(double));
				 memcpy(b,c, npages* sizeof(double));
				 memset(c, 0,npages * sizeof(double));

		 }

		pthread_barrier_wait(&mybarrier);
		// if you want test out without any barrier at the end but let all threads
		// do all of the work.
		// sleep(1);
	}


	return NULL;
}



void pagerank(node* list, size_t npages, size_t nedges, size_t nthreads, double dampener) {

	// Parallel Code
	double* matrix = (double*) malloc(npages*npages * sizeof(double));
	double* vector = (double*) malloc(npages * sizeof(double));
	worker_args args[nthreads];
	pthread_t thread_ids[nthreads];

	for(size_t i = 0; i < nthreads; i+= 4){
		args[i] = (worker_args){
			.id = i,
			.a = list,
			.matrix_setup = vector,
			.result = matrix,
			.npages = npages,
			.nthreads = nthreads,
			.dampener = dampener,
		};
		args[i+1] = (worker_args){
			.id = i+1,
			.a = list,
			.matrix_setup = vector,
			.result = matrix,
			.npages = npages,
			.nthreads = nthreads,
			.dampener = dampener,
		};
		args[i+2] = (worker_args){
			.id = i+2,
			.a = list,
			.matrix_setup = vector,
			.result = matrix,
			.npages = npages,
			.nthreads = nthreads,
			.dampener = dampener,
		};
		args[i+3] = (worker_args){
			.id = i+3,
			.a = list,
			.matrix_setup = vector,
			.result = matrix,
			.npages = npages,
			.nthreads = nthreads,
			.dampener = dampener,
		};
	}
	// Initalize the matrix
	for(size_t i = 0; i < nthreads; ++i){
		pthread_create(thread_ids + i, NULL, p_matrix_create, args + i);
	}
	for(size_t i = 0; i < nthreads; ++i){
		pthread_join(thread_ids[i], NULL);
	}
	// for(size_t i = 0; i < npages; i++){
	// 	for(size_t j = 0; j < npages; j++){
	// 		printf("At position number i %lu and position number j %lu, or position (%lu) has value %f\n", i, j, (i*npages + j),matrix[i*npages + j]);
	// 	}
	// }

	//Setting up the matrix multiplication
	double* vector_new = (double*) calloc(npages, sizeof(double));


	multi_args args1[nthreads];

	double counter[nthreads];
	// memset (counter,0, nthreads * sizeof(double));
	pthread_barrier_init(&mybarrier, NULL, nthreads);
	for(size_t i = 0; i < nthreads; i+=4){
		args1[i] = (multi_args){
			.id = i,
			.a = matrix,
			.b = vector,
			.c = vector_new,
			.npages = npages,
			.nthreads = nthreads,
			.counter = counter,
		};
		args1[i+1] = (multi_args){
			.id = i+1,
			.a = matrix,
			.b = vector,
			.c = vector_new,
			.npages = npages,
			.nthreads = nthreads,
			.counter = counter,
		};
		args1[i+2] = (multi_args){
			.id = i+2,
			.a = matrix,
			.b = vector,
			.c = vector_new,
			.npages = npages,
			.nthreads = nthreads,
			.counter = counter,
		};
		args1[i+3] = (multi_args){
			.id = i+3,
			.a = matrix,
			.b = vector,
			.c = vector_new,
			.npages = npages,
			.nthreads = nthreads,
			.counter = counter,
		};
	}

		for(size_t i = 0; i < nthreads; ++i){
			pthread_create(thread_ids + i, NULL, p_matrix_mult_1, args1 + i);
		}

		for(size_t i = 0; i < nthreads; ++i){
			pthread_join(thread_ids[i], NULL);
		}

	node* p_node_3 = list;
	for( size_t i = 0; i < npages; ++i){
		printf("%s %.8lf\n", p_node_3->page->name, vector[i]);
		p_node_3 = p_node_3->next;
	}

	free(vector_new);
	free(matrix);
	free(vector);
	pthread_barrier_destroy(&mybarrier);














	// // Sequential Code
	// double* a = (double*) malloc(npages*npages * sizeof(double));
	// node* p_node_1 = list;
	// node* p_node_2 = list;
	// bool ninlinks = false;
	//
	// double bracket = (((1 - dampener)/ npages) * 1);
	// double ratio = dampener * (double) 1/npages + bracket;
	// for(int i = 0; i < (int) npages; i++){
	// 	p_node_2 = list;
	// 	int pos = i*npages;
	// 	for(int j = 0; j < (int) npages; j++){
	// 		// Or find the outlinks of 1 as an array, another function to see the name matches with the name of the p+node_2?
	// 		ninlinks = s_noinlinks(p_node_1->page->inlinks, p_node_2->page->name);
	//
	// 		if(p_node_2->page->noutlinks == (size_t) 0){
	// 			a[pos + j] = ratio;
	// 		}
	// 		else if(ninlinks == true){
	// 			a[pos + j] = dampener * (double) 1/ p_node_2->page->noutlinks + bracket;
	// 		}
	// 		else{
	// 			a[pos + j] = bracket;
	// 		}
	// 		//printf("From that postion p_node_1 has index %s and p_node_2 has index %s\n\n", p_node_1->page->name, p_node_2->page->name);
	// 		//printf("At position number i %lu and position number j %lu has value %f\n", i, j, a[i*npages + j]);
	// 		p_node_2 = p_node_2 -> next;
	// 	}
	// 	p_node_1 = p_node_1->next;
	// }
	// double* b = (double*) malloc(npages * sizeof(double));
	//
	// // for(double* ptrDou = b; ptrDou < b + npages; ptrDou++){
	// // 	*ptrDou = (double) 1/npages;
	// // }
	// // d_memset((void*) b, 1/npages, npages);
	// for(int i = 0; i < (int) npages ;i++){
	// 	b[i] = (double) 1/npages;
	//
	// }
	// // printf("at letter b\n");
	// // for(size_t i = 0; i< npages; i++){
	// // 		printf("At position number i %lu has value %f\n", i, b[i]);
	// // }
	//
	// bool diff = false;
	// while (diff == false){
	// 	double * c = s_matrix_mult(a, b, npages);
	// 	diff = s_difference(b, c, npages);
	// 	memcpy(b,c, npages* sizeof(double));
	// 	free (c);
	// }
	//
	// 	node* p_node_3 = list;
	//  for( size_t i = 0; i < npages; i++){
	//  		printf("%s %.8lf\n", p_node_3->page->name, b[i]);
	// 		p_node_3 = p_node_3->next;
	//  }
	// free(a);
	// free(b);




	/*
	TODO

	- implement this function
	- implement any other necessary functions
	- implement any other useful data structures
	*/

}

/*
######################################
### DO NOT MODIFY BELOW THIS POINT ###
######################################
*/

int main(int argc, char** argv) {

	/*
	######################################################
	### DO NOT MODIFY THE MAIN FUNCTION OR HEADER FILE ###
	######################################################
	*/

	config conf;

	init(&conf, argc, argv);

	node* list = conf.list;
	size_t npages = conf.npages;
	size_t nedges = conf.nedges;
	size_t nthreads = conf.nthreads;
	double dampener = conf.dampener;

	pagerank(list, npages, nedges, nthreads, dampener);

	release(list);

	return 0;
}
